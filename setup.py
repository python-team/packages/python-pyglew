
from distutils.core import setup,Extension
import os

if os.name == 'nt': 
   data_files=[('Lib/site-packages', ['pyglew.dll', 'glew32.dll'])]
else:
   data_files=[('lib/python2.5/site-packages', ['pyglew.so', 'libGLEW.so'])]

setup(name='PyGLEW',
      version='0.1.2',
      description='GLEW bindings for Python',
      author='Calle Lejdfors',
      author_email='calle.lejdfors@cs.lth.se',
      url='http://www.cs.lth.se/~calle',
      packages=[],
      ext_modules=[],
      data_files=data_files)

