import sys

import pygame
from pygame.locals import *
from OpenGL.GLUT import *
from math import *
from Numeric import *
from pyglew import *


pygame.display.init()
surface = pygame.display.set_mode((512,512), OPENGL|DOUBLEBUF)

glewInit()
print "Vendor =", glGetString(GL_VENDOR)
print "Renderer =", glGetString(GL_RENDERER)
print "Version =", glGetString(GL_VERSION)

if not GLEW_ARB_pixel_buffer_object:
    print "Pixel buffer objects are not supported by your card!"
    sys.exit(1)
    
size = 100

pbo = glGenBuffers(1)
glBindBuffer(GL_PIXEL_PACK_BUFFER_ARB, pbo)
glBufferData(GL_PIXEL_PACK_BUFFER_ARB, size*3, None, GL_DYNAMIC_DRAW)

#glPixelStorei(GL_PACK_ALIGNMENT, 4)
#glPixelStorei(GL_UNPACK_ALIGNMENT, 4)

def polar(n):
    theta = n*2*pi/size
    return cos(theta), sin(theta), 0.0

data = ravel(array(map(polar, range(size)), Float32))

while 1:
    event = pygame.event.poll()
    if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
        sys.exit(0)

    glDrawPixels(size, 1, GL_RGB, GL_FLOAT, data.tostring())
    glReadPixels(0, 0, size, 1, GL_RGB, GL_FLOAT, 0);

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(-1, 1, -1, 1, 0.1, 10.0)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    glTranslatef(0,0,-1)

    glBindBuffer(GL_ARRAY_BUFFER, pbo)
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, 0);
    #glDrawArrays(GL_LINE_STRIP, 0, size);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, size);

    pygame.display.flip()
