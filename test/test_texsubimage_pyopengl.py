
import sys
# from pyglew import *
# from pyglut import *

from OpenGL.GL import *
from OpenGL.GLUT import *
from math import *

import pygame
from pygame.locals import *

from PIL import *
import PIL.Image

import bunny2

def renderBunny():
    glPushMatrix()

    glRotatef(t, 0, 1, 0);
    glRotatef(t*0.75, 1, 0, 0);

    #glVertexPointer(3, GL_FLOAT, 0,bunny2.vertices)
    #glNormalPointer(GL_FLOAT, 0, bunny2.normals)
    #glDrawArrays(GL_TRIANGLES, 0, len(bunny2.vertices)/3)
    glBegin(GL_TRIANGLES)
    for i in range(0,len(bunny2.vertices),3):
        n = bunny2.normals[i], bunny2.normals[i+1], bunny2.normals[i+2]
        v = bunny2.vertices[i], bunny2.vertices[i+1], bunny2.vertices[i+2],
        glNormal3f(*n)
        glVertex3f(*v)
    glEnd()
    glPopMatrix()

def renderTexture():
    glDisable(GL_LIGHTING)
    glEnable(GL_TEXTURE_2D)
    glBindTexture(GL_TEXTURE_2D, tex)

    glBegin(GL_QUADS)
    glTexCoord2f(0,0)
    glVertex3f(-1,-1,0)

    glTexCoord2f(1,0)
    glVertex3f(1,-1,0)

    glTexCoord2f(1,1)
    glVertex3f(1,1,0)

    glTexCoord2f(0,1)
    glVertex3f(-1,1,0)
    glEnd()

    glDisable(GL_TEXTURE_2D)
    glEnable(GL_LIGHTING)


def display():
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );
    renderBunny()

    glBindTexture(GL_TEXTURE_2D, tex)
    glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, w, h)
    
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    renderTexture()


w = h = 512

pygame.display.init()
surface = pygame.display.set_mode((w,h), OPENGL|DOUBLEBUF)

t = 0.0

# glewInit()

print "Vendor =", glGetString(GL_VENDOR)
print "Renderer =", glGetString(GL_RENDERER)
print "Version =", glGetString(GL_VERSION)


GL_TEXTURE_MAX_LEVEL = 0x813D

tex = glGenTextures(1)
glBindTexture(GL_TEXTURE_2D, tex);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_INT, None);


glEnable(GL_LIGHTING)
glEnable(GL_LIGHT0)
glEnable(GL_COLOR_MATERIAL)
glEnable(GL_CULL_FACE)
glEnable(GL_DEPTH_TEST)
# glCullFace(GL_BACK)

#glEnableClientState(GL_VERTEX_ARRAY)
#glEnableClientState(GL_NORMAL_ARRAY)

import timeit

def loop():
    global t
    t += 1
    event = pygame.event.poll()
    if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
        sys.exit(0)

    display()
    pygame.display.flip()

timer = timeit.Timer("loop()", "from __main__ import loop")
time = timer.timeit(10)/10

print "Loop time was:", time, "s"
