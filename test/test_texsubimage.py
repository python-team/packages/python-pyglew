
import sys
from pyglew import *

# from pyglut import *
from OpenGL.GLUT import *
from math import *

import pygame
from pygame.locals import *

from PIL import *
import PIL.Image

import bunny2

w = h = 1024

pygame.display.init()
surface = pygame.display.set_mode((w,h), OPENGL|DOUBLEBUF)

t = 0.0

glewInit()

print "Vendor =", glGetString(GL_VENDOR)
print "Renderer =", glGetString(GL_RENDERER)
print "Version =", glGetString(GL_VERSION)


tex = glGenTextures(1)
glBindTexture(GL_TEXTURE_2D, tex);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_INT, None);


def renderBunny():
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glFrustum(-1, 1, -1, 1, 2, 10)

    glMatrixMode(GL_MODELVIEW)
    glPushMatrix()
    glTranslatef(0,0,-3)

    glRotatef(t, 0, 1, 0);
    glRotatef(t*0.75, 1, 0, 0);
    
    glVertexPointer(3, GL_FLOAT, 0,bunny2.vertices)
    glNormalPointer(GL_FLOAT, 0, bunny2.normals)
    glDrawArrays(GL_TRIANGLES, 0, len(bunny2.vertices)/3)

    glPopMatrix()

def renderTexture():
    glDisable(GL_LIGHTING)
    glEnable(GL_TEXTURE_2D)
    glBindTexture(GL_TEXTURE_2D, tex)


    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(-1, 1, -1, 1, 0, 1)
    
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    glBegin(GL_QUADS)
    glTexCoord2f(0,0)
    glVertex3f(-1,-1,0)

    glTexCoord2f(1,0)
    glVertex3f(1,-1,0)

    glTexCoord2f(1,1)
    glVertex3f(1,1,0)

    glTexCoord2f(0,1)
    glVertex3f(-1,1,0)
    glEnd()

    glDisable(GL_TEXTURE_2D)
    glEnable(GL_LIGHTING)


def display():
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );
    renderBunny()

    glBindTexture(GL_TEXTURE_2D, tex)
    glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, w, h)
    
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    renderTexture()



glEnable(GL_LIGHTING)
glEnable(GL_LIGHT0)
glEnable(GL_COLOR_MATERIAL)
glEnable(GL_DEPTH_TEST)
glCullFace(GL_CW)

glEnableClientState(GL_VERTEX_ARRAY)
glEnableClientState(GL_NORMAL_ARRAY)

import timeit

def loop():
    global t
    t += 1
    event = pygame.event.poll()
    if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
        sys.exit(0)

    display()
    pygame.display.flip()

timer = timeit.Timer("loop()", "from __main__ import loop")
time = timer.timeit(1000)/1000

print "Loop time was:", time, "s"
