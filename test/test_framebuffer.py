
import sys
from pyglew import *

# from pyglut import *
from OpenGL.GLUT import *
from math import *

import pygame
from pygame.locals import *

from PIL import *
import PIL.Image

import bunny2

def renderBunny():
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glFrustum(-1, 1, -1, 1, 2, 10)

    glMatrixMode(GL_MODELVIEW)
    
    glPushMatrix()
    glTranslatef(0,0,-3)
    
    glRotatef(t, 0, 1, 0);
    glRotatef(t*0.75, 1, 0, 0);
    
    glVertexPointer(3, GL_FLOAT, 0,bunny2.vertices)
    glNormalPointer(GL_FLOAT, 0, bunny2.normals)
    glDrawArrays(GL_TRIANGLES, 0, len(bunny2.vertices)/3)

    glPopMatrix()

def renderTexture():
    glEnable(GL_TEXTURE_2D)
    glBindTexture(GL_TEXTURE_2D, tex)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(-1, 1, -1, 1, 0, 1)
    
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    glBegin(GL_QUADS)
    glTexCoord2f(0,0)
    glVertex3f(-1,-1,0)

    glTexCoord2f(1,0)
    glVertex3f(1,-1,0)

    glTexCoord2f(1,1)
    glVertex3f(1,1,0)

    glTexCoord2f(0,1)
    glVertex3f(-1,1,0)
    glEnd()

    glDisable(GL_TEXTURE_2D)


def display():
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fb);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );

    renderBunny()

    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    renderTexture()


w = h = 1024

pygame.display.init()
surface = pygame.display.set_mode((w,h), OPENGL|DOUBLEBUF)

t = 0.0

glewInit()

print "Vendor =", glGetString(GL_VENDOR)
print "Renderer =", glGetString(GL_RENDERER)
print "Version =", glGetString(GL_VERSION)


tex = glGenTextures(1)
glBindTexture(GL_TEXTURE_2D, tex);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_INT, None);


fb = glGenFramebuffersEXT(1);
glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fb);
glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,
                          GL_COLOR_ATTACHMENT0_EXT,
                          GL_TEXTURE_2D, tex, 0);

## depth buffer
depth = glGenRenderbuffersEXT(1);
glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depth);
glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT,
                         GL_DEPTH_COMPONENT24, w, h);
glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT,
                             GL_DEPTH_ATTACHMENT_EXT,
                             GL_RENDERBUFFER_EXT, depth);
  
# stencil buffer
stencil = glGenRenderbuffersEXT(1);
glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, stencil);
glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT,
                         GL_STENCIL_INDEX16_EXT, w, h);
glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT,
                             GL_STENCIL_ATTACHMENT_EXT,
                             GL_RENDERBUFFER_EXT, stencil);

glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

glEnable(GL_LIGHTING)
glEnable(GL_LIGHT0)
glEnable(GL_COLOR_MATERIAL)
glEnable(GL_CULL_FACE)
glEnable(GL_DEPTH_TEST)

glEnableClientState(GL_VERTEX_ARRAY)
glEnableClientState(GL_NORMAL_ARRAY)

glLightModelfv(GL_LIGHT_MODEL_AMBIENT, [1,0,0,0])


import timeit

def loop():
    global t
    t += 1
    event = pygame.event.poll()
    if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
        sys.exit(0)

    display()
    pygame.display.flip()

timer = timeit.Timer("loop()", "from __main__ import loop")
time = timer.timeit(1000)/1000

print "Loop time was:", time, "s"
