import sys

import pygame
from pygame.locals import *
from math import *
from Numeric import *
from pyglew import *


pygame.display.init()
surface = pygame.display.set_mode((512,512), OPENGL|DOUBLEBUF)

glewInit()
print "Vendor =", glGetString(GL_VENDOR)
print "Renderer =", glGetString(GL_RENDERER)
print "Version =", glGetString(GL_VERSION)
   

def polar(n):
    theta = n*2*pi/100
    return cos(theta), sin(theta), 0.0

data = ravel(array(map(polar, range(100)), Float32))

while 1:
    event = pygame.event.poll()
    if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
        sys.exit(0)

    glDrawPixels(100, 1, GL_RGB, GL_FLOAT, data.tostring())
    str = glReadPixels(0, 0, 100, 1, GL_RGB, GL_FLOAT);

    arr = fromstring(str, Float32)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(-1, 1, -1, 1, 0.1, 10.0)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    glTranslatef(0,0,-1)
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, list(ravel(arr)));
    glDrawArrays(GL_POINTS, 0, 100);

    pygame.display.flip()
