
//////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2005 Carl Johan Lejdfors                                               //
//                                                                                      //
// This program is free software; you can redistribute it and/or modify                 //
// it under the terms of the GNU General Public License as published by                 //
// the Free Software Foundation; either version 2 of the License, or                    //
// (at your option) any later version.                                                  //
//                                                                                      //
// This program is distributed in the hope that it will be useful,                      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of                       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        //
// GNU General Public License for more details.                                         //
//                                                                                      //
// You should have received a copy of the GNU General Public License                    //
// along with this program; if not, write to the Free Software                          //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA            //
//                                                                                      //
// Please send all bugreports to: Carl Johan Lejdfors <calle.lejdfors@cs.lth.se>        //
//////////////////////////////////////////////////////////////////////////////////////////

#include "pyglew_exception.hh"

template<class T>
inline T unpack(PyObject* object) throw(pyglew_exception)
{
  assert(false); 
  return T();
}

template<>
inline GLenum unpack<GLenum>(PyObject* object)
{
  return (GLenum)PyInt_AsLong(object);
}

template<>
inline GLsizei unpack<GLsizei>(PyObject* object)
{
  return (GLsizei)PyInt_AsLong(object);
}


template<>
inline GLfloat unpack<GLfloat>(PyObject* object)
{
  return (GLfloat)PyFloat_AsDouble(object);
}

template<>
inline GLdouble unpack<GLdouble>(PyObject* object)
{
  return (GLdouble)PyFloat_AsDouble(object);
}


#if __WORDSIZE == 64
template<>
inline GLsizeiptrARB unpack<GLsizeiptrARB>(PyObject* object)
{
  return (GLsizeiptrARB)PyInt_AsLong(object);
}
#endif /* __WORDSIZE == 64 */


template<>
inline GLfloat* unpack<GLfloat*>(PyObject* object)
{
  // return (GLfloat)PyFloat_AsDouble(object);
  void *buffer;
  int len;
  if ( PyObject_AsWriteBuffer(object, &buffer, &len) < 0 ) {
    throw pyglew_exception("In 'unpack<GLfloat*>' unable to access write buffer.");
  } 

  return reinterpret_cast<GLfloat*>(buffer);
}



