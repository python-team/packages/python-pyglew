
//////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2005 Carl Johan Lejdfors                                               //
//                                                                                      //
// This program is free software; you can redistribute it and/or modify                 //
// it under the terms of the GNU General Public License as published by                 //
// the Free Software Foundation; either version 2 of the License, or                    //
// (at your option) any later version.                                                  //
//                                                                                      //
// This program is distributed in the hope that it will be useful,                      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of                       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        //
// GNU General Public License for more details.                                         //
//                                                                                      //
// You should have received a copy of the GNU General Public License                    //
// along with this program; if not, write to the Free Software                          //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA            //
//                                                                                      //
// Please send all bugreports to: Carl Johan Lejdfors <calle.lejdfors@cs.lth.se>        //
//////////////////////////////////////////////////////////////////////////////////////////

#include <exception>
#include <string>

#ifndef __pyglew_exception_hh__
#define __pyglew_exception_hh__

class pyglew_exception : public std::exception {
public:  
  pyglew_exception(const char* fmt, ...) {
    char buffer[1025];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf( buffer, 1025, fmt, ap);
    
    _message = buffer;
  }

  pyglew_exception(const std::string& message) 
    : _message(message) { }

  ~pyglew_exception() throw () {}

  const char* what() const throw() { return _message.c_str(); }
  
protected:
  std::string _message;
};

#endif /* __pyglew_exception_hh__ */
